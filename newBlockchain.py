#From https://www.youtube.com/watch?v=MViBvQXQ3mM
#     https://github.com/llSourcell/Simple_Blockchain_in_5_Minutes/blob/master/blockchain%20(1).ipynb
#Following the structure from https://anders.com/blockchain/

#TIMESTAMPS
import datetime
#HASHING
import hashlib
#GUI
import tkinter as tk

#things needed for continuation:

#Tokens
class Token:
    moneyAmount = 0.0
    moneyFrom = "No one"
    moneyTo = "No one"
    
    #for printing info. Not working when in
    def __str__(self):
        #print the token
        return "Amount: ${}, From: {}, To: {} \n".format(str(self.moneyAmount), self.moneyFrom, self.moneyTo)



#defining the 'block' data structure
class Block:
    #has 7 attributes
    #1 number of the block
    blockNo = 0

    #2 make a list
    initToken = Token()
    data = [initToken]

    #3 pointer to the next block
    next = None

    #4 the hash of this block
    blockHash = None

    #5 nonce
    nonce = 0

    #6 store the hash of previous
    prevHash = 0x0

    #7 timestamp
    timestamp = datetime.datetime.now()
    
    #Funct to compute hash
    def hash(self):
        h = hashlib.sha256()
        h.update(
            str(self.nonce).encode('utf-8')+
            str(self.data).encode('utf-8')+
            str(self.prevHash).encode('utf-8')+
            str(self.timestamp).encode('utf-8')+
            str(self.blockNo).encode('utf-8')
        )
        return h.hexdigest()

    #Funct to add the transaction to the data list
    def newTransaction(self, newToken):
        self.data.append(newToken)
        print(self.data[-1])

    #for printing the block
    def __str__(self):
        #print out the value of block
        TransactionHistory = ""
        for x in range(len(self.data)):
            TransactionHistory=TransactionHistory+str(self.data[x])+"\n"

        return "Block Hash: " + str(self.hash()) + "\nBlockNo: " + str(self.blockNo) + "\nBlock Data: " + str(self.data) + "\nHashes: " + str(self.nonce) + "\n-----------------"



#define blockchain 
class Blockchain:

    #The max number we can store
    maxNonce = 2**32
    #mining difficulty
    diff = 10
    #target hash for mining
    target = 2 ** (256-diff)

    #generate the first block
    block = Block()

    #set as head
    head = block

    #For traversing through the blockchain
    temp = head

    #adds block to chain
    def add(self,block):
        #set current block's hash as new prevHash
        block.prevHash = self.block.hash()
        #Increment block number
        block.blockNo = self.block.blockNo + 1

        #Set new block equal to itself, now the head
        self.block.next = block
        self.block = self.block.next

    #determines whether or not block can be added
    def mine(self,block):
        for n in range(self.maxNonce):
            if int(block.hash(), 16) <= self.target:
                #if value is less than target, add new block
                self.add(block)
                print(block)
                break
            else:
                block.nonce += 1

    #for printing blocks in Blockchain
    def printBlocks(self):
        self.temp = self.head
        while self.temp != None:
            print(self.temp)
            self.temp = self.temp.next



#class to make
# contains a name, coinbase, and blockchain as an account 
class User:
    name = "No one"
    coinbase = 0.0

    account = Blockchain()

    #initializing the name and coinbase
    def __init__(self):
        self.name = input("Create your Username: ")
        self.coinbase = float(input("Enter initial deposit: "))
        print("\n\n")

    #for printing the user
    def __str__(self):
        return "Username is " + self.name + "\n" + "Coinbase: $" + str(self.coinbase) + "\n"


    #adding a new transaction to the current block 
    def newTrans(self):
        newToken = Token()
        newToken.moneyAmount = float(input("Enter the Amount: "))
        newToken.moneyFrom = input("Enter where from: ")
        newToken.moneyTo = input("Enter where to: ")
        self.account.head.newTransaction(newToken)
        if newToken.moneyFrom == self.name:
            self.coinbase = self.coinbase - newToken.moneyAmount
        else:
            self.coinbase = self.coinbase + newToken.moneyAmount

#have global user
user = User()
#GUI Implementation
# basic code from:
#   https://loctv.wordpress.com/2016/09/28/python-tkinter-multiple-windowsframes/


#Degining the GUI window and giving title name
win = tk.Tk()
win.title("Banana Bank")

#defining and adding a frame of the window with a label
TopFrame = tk.Frame(win)
TopFrame.pack()
TopLabel = tk.Label(TopFrame, text= "Welcome to Banana Bank!")

#defining buttons
 
#button that prints transactions to terminal
DepositBut = tk.Button(TopFrame, text="Print Transactions", command=lambda:print(user.account.printBlocks()))

#button that adds transaction from terminal
TransBut = tk.Button(TopFrame, text="Transaction", command=user.newTrans)

#button that prints the user and coinbase to terminal
AccountInfo = tk.Button(TopFrame, text="Account Info", command=lambda:print(user))


#adding buttons and lable to frame
TopLabel.pack(side = tk.TOP)
DepositBut.pack(side = tk.LEFT)
TransBut.pack(side =tk.RIGHT)
AccountInfo.pack(side = tk.BOTTOM)

#display and interact with window
win.mainloop()


