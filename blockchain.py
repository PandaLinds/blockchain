#From https://www.youtube.com/watch?v=MViBvQXQ3mM
#     https://github.com/llSourcell/Simple_Blockchain_in_5_Minutes/blob/master/blockchain%20(1).ipynb
#Following the structure from https://anders.com/blockchain/

#TIMESTAMPS
import datetime
#HASHING
import hashlib

#things needed for continuation:

#Tokens
class Token:
    moneyAmount = 0.0
    moneyFrom = "No one"
    moneyTo = "No one"
    
    def __str__(self):
        #print the token
        return "Amount: ${}, From: {}, To: {} \n".format(str(self.moneyAmount), self.moneyFrom, self.moneyTo)



#defining the 'block' data structure
class Block:
    #has 7 attributes
    #1 number of the block
    blockNo = 0

    #2 make a list
    data = []

    #3 pointer to the next block
    next = None

    #4 the hash of this block
    blockHash = None

    #5 nonce
    nonce = 0

    #6 store the hash of previous
    prevHash = 0x0

    #7 timestamp
    timestamp = datetime.datetime.now()

    #initialize a block 
    def __init__(self,data):
        self.data = data
    
    #Funct to compute hash
    def hash(self):
        h = hashlib.sha256()
        h.update(
            str(self.nonce).encode('utf-8')+
            str(self.data).encode('utf-8')+
            str(self.prevHash).encode('utf-8')+
            str(self.timestamp).encode('utf-8')+
            str(self.blockNo).encode('utf-8')
        )
        return h.hexdigest()

    def newTransaction(self,newTransaction):
        self.data.append(newTransaction)
        print(self.data[-1])

    def __str__(self):
        #print out the value of block
        return "Block Hash: " + str(self.hash()) + "\nBlockNo: " + str(self.blockNo) + "\nBlock Data: " + str(self.data) + "\nHashes: " + str(self.nonce) + "\n-----------------"


#define block chain 
class Blockchain:

    #The max number we can store
    maxNonce = 2**32
    #mining difficulty
    diff = 10
    #target hash for mining
    target = 2 ** (256-diff)

    #generate the first block
    block = Block(Token())

    #set as head
    head = block

    #For traversing through the blockchain
    temp = head

    #adds block to chain
    def add(self,block):
        #set current block's hash as new prevHash
        block.prevHash = self.block.hash()
        #Increment block number
        block.blockNo = self.block.blockNo + 1

        #Set new block equal to itself, now the head
        self.block.next = block
        self.block = self.block.next

    #determines whether or not block can be added
    def mine(self,block):
        for n in range(self.maxNonce):
            if int(block.hash(), 16) <= self.target:
                #if value is less than target, add new block
                self.add(block)
                print(block)
                break
            else:
                block.nonce += 1

    def printBlocks(self):
        while self.temp != None:
            print(self.temp)
            self.temp = self.temp.next
#tie into blockchain
class User:
    name = "No one"
    coinbase = 0.0

    account = Blockchain()

    def __init__(self):
        self.name = input("Create your Username: ")
        self.coinbase = input("Enter initial deposit: ")
        print("\n\n")

    def __str__(self):
        return "Username is " + self.name + "\n" + "Coinbase: $" + str(self.coinbase) + "\n"

#bank class
class Bank:
    accounts=[]
    cashAmount = 0.0
    transactionHistory = []

    def addAccount():
        #add to account list

    def updateBank():
        #update cash and transaction


def userInterface(self):
    while 1:
        #print(user.name)
        choice = input("1: Transaction \n2: Deposit 3: Transfer to Other User\n4: Switch User\n5: exit\n")
        if choice == 3:
            print("Goodbye\n\n")

        elif choice == 1:
            moneyAmount = input("\nEnter the amount: ")
            deposName = input("\nEnter the deposit name: ")
            self.coinbase = user1.coinbase + moneyAmount
            #add transaction
            

